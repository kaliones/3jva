package com.supinfo.commerce.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.supinfo.commerce.dao.interfaces.SaleAdDAOInt;
import com.supinfo.commerce.entities.Category;
import com.supinfo.commerce.entities.SaleAd;
import com.supinfo.commerce.entities.User;

public class SaleAdDAO extends AbstractJPA<SaleAd> implements SaleAdDAOInt {

	public SaleAdDAO() {
		super(SaleAd.class);
	}

	private String getQueryAll() {
		return "SELECT c FROM " + classT.getName() + " c";
	}

	public List<SaleAd> getAll(int page) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			int maxResult = 10;
			Query query = em.createQuery(getQueryAll());
			query.setFirstResult(maxResult * page - maxResult);
			query.setMaxResults(maxResult);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<SaleAd> getAll(int page, int maxResult) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			Query query = em.createQuery(getQueryAll() + " order by c.createdAt");
			query.setFirstResult(maxResult * page - maxResult);
			query.setMaxResults(maxResult);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<SaleAd> getAllInSale(int page, int maxResult) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			Query query = em.createQuery(getQueryAll() + " WHERE c.activated = 1 and c.sale = 0 order by c.createdAt");
			query.setFirstResult(maxResult * page - maxResult);
			query.setMaxResults(maxResult);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<SaleAd> getAllByUser(User user) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			Query query = em.createQuery(
					getQueryAll() + " WHERE c.activated = 1 and c.seller = " + user.getId() + " order by c.createdAt");
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<SaleAd> getAllByUserActivated(User user) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			Query query = em.createQuery(getQueryAll() + " WHERE c.seller = " + user.getId() + " order by c.createdAt");
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<SaleAd> getAllByUser(User user, int page) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			int maxResult = 10;
			Query query = em.createQuery(
					getQueryAll() + " WHERE c.activated = 1 and c.seller = " + user.getId() + " order by c.createdAt");
			query.setFirstResult(maxResult * page - maxResult);
			query.setMaxResults(maxResult);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<SaleAd> getAllByUserActivated(User user, int page) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			int maxResult = 10;
			Query query = em.createQuery(getQueryAll() + " WHERE c.seller = " + user.getId() + " order by c.createdAt");
			query.setFirstResult(maxResult * page - maxResult);
			query.setMaxResults(maxResult);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<SaleAd> getAllByCategory(Category catego, int page, int maxResult) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			String categoWhere = "null";
			if (catego != null) {
				categoWhere = String.valueOf(catego.getId());
			}
			Query query = em.createQuery(getQueryAll() + " WHERE c.activated = 1 and c.sale = 0 and  c.category = "
					+ categoWhere + " order by c.createdAt");
			query.setFirstResult(maxResult * page - maxResult);
			query.setMaxResults(maxResult);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<SaleAd> getAllByCategory(Category catego, int page, int maxResult, String search) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			String categoWhere = "null";
			if (catego != null) {
				categoWhere = String.valueOf(catego.getId());
			}
			search = search.toLowerCase();
			Query query = em.createQuery(getQueryAll() + " WHERE c.activated = 1 and c.sale = 0 and c.category = "
					+ categoWhere
					+ " and (lower(c.name ) LIKE :search OR lower(c.description ) LIKE :search) order by c.createdAt");
			query.setParameter("search", "%" + search + "%");
			query.setFirstResult(maxResult * page - maxResult);
			query.setMaxResults(maxResult);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<SaleAd> getAllBySearch(String search, int page, int maxResult) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			search = search.toLowerCase();
			Query query = em.createQuery(getQueryAll()
					+ " WHERE c.activated = 1 and c.sale = 0 and (lower(c.name ) LIKE :search OR lower(c.description ) LIKE :search) order by c.createdAt");
			query.setParameter("search", "%" + search + "%");
			query.setFirstResult(maxResult * page - maxResult);
			query.setMaxResults(maxResult);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public long countAll(User user) {
		EntityManager em = entityManagerF.createEntityManager();
		Query query = em.createQuery(
				"SELECT count(*) FROM " + classT.getName() + " c WHERE c.activated = 1 and c.seller = " + user.getId());
		try {
			return (long) query.getSingleResult();
		} catch (Exception e) {
			return 0;
		} finally {
			em.close();
		}
	}

	public long countAllActivated(User user) {
		EntityManager em = entityManagerF.createEntityManager();
		Query query = em
				.createQuery("SELECT count(*) FROM " + classT.getName() + " c WHERE c.seller = " + user.getId());
		try {
			return (long) query.getSingleResult();
		} catch (Exception e) {
			return 0;
		} finally {
			em.close();
		}
	}

	public long countAll() {
		EntityManager em = entityManagerF.createEntityManager();
		Query query = em.createQuery("SELECT count(*) FROM " + classT.getName() + " c");
		try {
			return (long) query.getSingleResult();
		} catch (Exception e) {
			return 0;
		} finally {
			em.close();
		}
	}

	public long countAllInSale() {
		EntityManager em = entityManagerF.createEntityManager();
		Query query = em
				.createQuery("SELECT count(*) FROM " + classT.getName() + " c WHERE  c.activated = 1 and c.sale = 0");
		try {
			return (long) query.getSingleResult();
		} catch (Exception e) {
			return 0;
		} finally {
			em.close();
		}
	}

	public long countAllSale() {
		EntityManager em = entityManagerF.createEntityManager();
		Query query = em.createQuery("SELECT count(*) FROM " + classT.getName() + " c WHERE c.sale = 1");
		try {
			return (long) query.getSingleResult();
		} catch (Exception e) {
			return 0;
		} finally {
			em.close();
		}
	}

	public long countAllInSale(Category category) {
		EntityManager em = entityManagerF.createEntityManager();
		String idCategory = "null";
		if (category != null) {
			idCategory = String.valueOf(category.getId());
		}
		Query query = em.createQuery("SELECT count(*) FROM " + classT.getName()
				+ " c WHERE c.activated = 1 and c.sale = 0 and c.category = " + idCategory);
		try {
			return (long) query.getSingleResult();
		} catch (Exception e) {
			return 0;
		} finally {
			em.close();
		}
	}

	public long countAllInSale(Category category, String search) {
		EntityManager em = entityManagerF.createEntityManager();
		String idCategory = "null";
		if (category != null) {
			idCategory = String.valueOf(category.getId());
		}
		Query query = em.createQuery("SELECT count(*) FROM " + classT.getName()
				+ " c WHERE c.activated = 1 and c.sale = 0 and (lower(c.name ) LIKE :search OR lower(c.description ) LIKE :search) and c.category = "
				+ idCategory);
		query.setParameter("search", "%" + search + "%");
		try {
			return (long) query.getSingleResult();
		} catch (Exception e) {
			return 0;
		} finally {
			em.close();
		}
	}

	public long countAllInSale(String search) {
		EntityManager em = entityManagerF.createEntityManager();
		Query query = em.createQuery("SELECT count(*) FROM " + classT.getName()
				+ " c WHERE c.activated = 1 and  c.sale = 0 and (lower(c.name ) LIKE :search OR lower(c.description ) LIKE :search)");
		query.setParameter("search", "%" + search + "%");
		try {
			return (long) query.getSingleResult();
		} catch (Exception e) {
			return 0;
		} finally {
			em.close();
		}
	}
}

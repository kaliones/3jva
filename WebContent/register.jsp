<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="template/startHTML.jsp"></jsp:include>
<title>Register</title>
</head>
<body>
	<jsp:include page="template/navbar.jsp"></jsp:include>
	<jsp:include page="template/header.jsp"></jsp:include>
	<div class="row">
		<div class="col-sm-3"></div>
		<form method="post" action="register" class="col-sm-6">
			<fieldset>
				<legend class="text-center">Register</legend>
				<p>You can register via this form.</p>

				<div
					class="form-group required	<c:if test="${not empty errors['email']}">has-error</c:if>">
					<label class="control-label" for="email">E-mail adress</label> <input
						class="form-control" type="email" id="email" name="email"
						value="${user.email}" /> <span class="help-block">${errors['email']}</span>
				</div>

				<div
					class="form-group required	<c:if test="${not empty errors['username']}">has-error</c:if>">
					<label class="control-label" for="username">Username</label> <input
						class="form-control" type="text" id="username" name="username"
						value="${user.username}" /> <span class="help-block">${errors['username']}</span>
				</div>

				<div
					class="form-group required	<c:if test="${not empty errors['lastName']}">has-error</c:if>">
					<label class="control-label" for="lastName">Last name</label> <input
						class="form-control" type="text" id="lastName" name="lastName"
						value="${user.lastName}" /> <span class="help-block">${errors['lastName']}</span>
				</div>
				<div
					class="form-group required	<c:if test="${not empty errors['firstName']}">has-error</c:if>">
					<label class="control-label" for="firstName">First name</label> <input
						class="form-control" type="text" id="firstName" name="firstName"
						value="${user.firstName}" /> <span class="help-block">${errors['firstName']}</span>
				</div>

				<div
					class="form-group required	<c:if test="${not empty errors['phone']}">has-error</c:if>">
					<label class="control-label" for="phone">Phone number</label> <input
						class="form-control" type="tel" id="phone" name="phone"
						value="${user.phone}" /> <span class="help-block">${errors['phone']}</span>
				</div>

				<div
					class="form-group required	<c:if test="${not empty errors['address']}">has-error</c:if>">
					<label class="control-label" for="address">Address</label> <input
						class="form-control" type="text" id="address" name="address"
						value="${user.address}" /> <span class="help-block">${errors['address']}</span>
				</div>

				<div
					class="form-group required	<c:if test="${not empty errors['password']}">has-error</c:if>">
					<label class="control-label" for="password">Password</label> <input
						class="form-control" type="password" id="password" name="password"
						value="" size="20" maxlength="20" /> <span class="help-block">${errors['password']}</span>
				</div>

				<div
					class="form-group required	<c:if test="${not empty errors['passwordConfirmation']}">has-error</c:if>">
					<label class="control-label" for="passwordConfirmation">Password
						Confirmation</label> <input class="form-control" type="password"
						id="passwordConfirmation" name="passwordConfirmation" value=""
						size="20" maxlength="20" /> <span class="help-block">${errors['passwordConfirmation']}</span>
				</div>
				<input type="submit" value="Register"
					class="btn btn-primary pull-right" />
			</fieldset>
		</form>
	</div>
	
	<jsp:include page="template/footer.jsp"></jsp:include>
</body>
</html>
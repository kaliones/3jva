package com.supinfo.commerce.dao;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.supinfo.commerce.dao.interfaces.GroupDAOInt;
import com.supinfo.commerce.entities.Group;

public class GroupDAO extends AbstractJPA<Group> implements GroupDAOInt {

	public GroupDAO() {
		super(Group.class);
	}

	public Group getByKey(String key) {
		EntityManager em = entityManagerF.createEntityManager();
		Query query = em.createQuery("SELECT c FROM " + classT.getName() + " c WHERE c.keyName = :key");
		query.setParameter("key", key);
		try {
			return (Group) query.getSingleResult();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}
}

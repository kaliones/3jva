package com.supinfo.commerce.servlets.salead;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.SaleAdDAO;
import com.supinfo.commerce.entities.SaleAd;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class SaleAd;
 */
@WebServlet(name = "changeStatusSaleAd", urlPatterns = { "/auth/salead/changestatus" })
public class ChangeStatusSaleAdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangeStatusSaleAdServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SaleAdDAO saleAdDAO = DAOFactory.getSaleAdDAO();
		SaleAd saleAd;
		// if admin, edit all profil
		long idSaleAd = -1;
		User userConnected = (User) request.getAttribute("userConnected");
		try {
			idSaleAd = Long.parseLong(request.getParameter("id"));
			saleAd = saleAdDAO.get(idSaleAd);
			if (saleAd == null) {
				request.getSession().setAttribute("msgError", "Sale ad not found.");
				if ((userConnected.getGroup().getKeyName()).equals("ADMIN")) {
					response.sendRedirect(request.getContextPath() + "/admin/saleads");
				} else {
					response.sendRedirect(request.getContextPath() + "/auth/saleads/manager");
				}
				return;
			}
			if ((userConnected.getGroup().getKeyName()).equals("ADMIN")
					|| (saleAd.getSeller().getId()) == userConnected.getId()) {
				saleAd.setActivated(!saleAd.getActivated());
				saleAdDAO.update(saleAd);
				request.getSession().setAttribute("msgSuccess", "Sale ad has been updated.");
				if ((userConnected.getGroup().getKeyName()).equals("ADMIN")) {
					response.sendRedirect(request.getContextPath() + "/admin/saleads");
				} else {
					response.sendRedirect(request.getContextPath() + "/auth/saleads/manager");
				}
				return;
			} else {
				request.getSession().setAttribute("msgError", "You do not have access to this.");
				response.sendRedirect(request.getContextPath() + "/index");
				return;
			}
		} catch (Exception e) {
			request.getSession().setAttribute("msgError", "Sale ad not found.");
			response.sendRedirect(request.getContextPath() + "/admin/saleads");
			return;
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

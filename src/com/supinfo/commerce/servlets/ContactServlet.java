package com.supinfo.commerce.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.SaleAdDAO;
import com.supinfo.commerce.entities.SaleAd;
import com.supinfo.commerce.entities.User;

@WebServlet(name = "contact", urlPatterns = { "/contact" })
public class ContactServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ContactServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String name = (String) request.getParameter("name");
		String phone = (String) request.getParameter("phone");
		String message = (String) request.getParameter("message");
		String email = (String) request.getParameter("email");
		String idSaleAd = (String) request.getParameter("idSaleAd");
		Map<String, String> errors = new HashMap<String, String>();
		Map<String, String> res = new HashMap<String, String>();
		res.put("name", name);
		res.put("phone", phone);
		res.put("message", message);
		res.put("email", email);
		if (email == null || email.trim() == "") {
			errors.put("email", "Email address is required");
		} else if (!User.isValideEmail(email)) {
			errors.put("email", "Email address is not a valid email address");
		}
		if (phone == null || phone.trim() == "") {
			errors.put("phone", "Phone is required");
		} else if (!User.isValidePhone(phone)) {
			errors.put("phone", "Phone is not valid");
		}
		if (name == null || name.trim() == "") {
			errors.put("name", "Last name is required");
		}
		if (message == null || message.trim() == "") {
			errors.put("message", "Message is required");
		}

		request.setAttribute("errors", errors);
		request.setAttribute("res", res);
		SaleAd saleAd;
		try {
			SaleAdDAO saleAdDAO = DAOFactory.getSaleAdDAO();
			saleAd = saleAdDAO.get(Long.parseLong(idSaleAd));
			request.setAttribute("id", idSaleAd);

			if (saleAd == null) {
				request.getSession().setAttribute("msgError", "Sale ad not found.");
				response.sendRedirect(request.getContextPath() + "/index");
				return;
			}
		} catch (Exception e) {
			request.getSession().setAttribute("msgError", "Sale ad not found.");
			response.sendRedirect(request.getContextPath() + "/index");
			return;
		}
		if (errors.size() > 0) {
			request.getRequestDispatcher("/salead").forward(request, response);
			return;
		} else {
			try {
				Properties properties = System.getProperties();
				properties.put("mail.smtp.auth", "true");
				properties.put("mail.smtp.starttls.enable", "true");
				properties.put("mail.smtp.host", "smtp.gmail.com");
				properties.put("mail.smtp.port", "587");
				String emailSender = "3jvaprojet@gmail.com";
				Session session = Session.getInstance(properties, new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(emailSender, "3JVAAlexis");
					}
				});
				MimeMessage messageMail = new MimeMessage(session);
				messageMail.setFrom(new InternetAddress(emailSender));
				messageMail.addRecipient(Message.RecipientType.TO, new InternetAddress(saleAd.getSeller().getEmail()));
				messageMail.setSubject("Contact for your ad : " + saleAd.getName());
				messageMail.setText(
						"Username : " + name + " Phone : " + phone + " Mail : " + email + " Message : " + message);

				Transport.send(messageMail);
				request.removeAttribute("res");
				request.getSession().setAttribute("msgSuccess", "Message Send.");
			} catch (Exception mex) {
				request.getSession().setAttribute("msgError", "Error send Mail.");
				request.getRequestDispatcher("/salead").forward(request, response);
				return;
			}
			request.getRequestDispatcher("/salead").forward(request, response);
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

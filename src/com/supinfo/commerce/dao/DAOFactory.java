package com.supinfo.commerce.dao;

public class DAOFactory {

	private DAOFactory() {
	}

	public static UserDAO getUserDAO() {
		return new UserDAO();
	}

	public static CategoryDAO getCategoryDAO() {
		return new CategoryDAO();
	}

	public static SaleAdDAO getSaleAdDAO() {
		return new SaleAdDAO();
	}

	public static GroupDAO getGroupDAO() {
		return new GroupDAO();
	}
}

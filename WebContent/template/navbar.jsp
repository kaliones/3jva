<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<nav class="navbar navbar-default">
	<div class="container container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${ pageContext.request.contextPath }/index">SupSales</a>
		</div>
		<div class="collapse navbar-collapse" id="navbar-collapse-1">
			<ul class="nav navbar-nav">
				<c:choose>
					<c:when test="${ sessionScope.username != null }">
						<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Sale Ad<span class="caret"></span></a>
							<ul class="dropdown-menu">

								<li><a href="${ pageContext.request.contextPath }/search">List</a></li>
								<li><a href="${ pageContext.request.contextPath }/auth/salead/add"> New add</a></li>
								<li><a href="${ pageContext.request.contextPath }/auth/saleads/manager"> My adds</a></li>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">My Account<span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="${ pageContext.request.contextPath }/auth/user/update">Manage your account </a></li>
								<li><a href="${ pageContext.request.contextPath }/logout">Logout</a></li>
							</ul></li>

						<c:if test="${(requestScope.userConnected.group.name) != null && (requestScope.userConnected.group.keyName) == 'ADMIN'}">
							<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Admin<span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="${ pageContext.request.contextPath }/admin/users"> User</a></li>
									<li><a href="${ pageContext.request.contextPath }/admin/saleads"> Sale Ad</a></li>
								</ul></li>
						</c:if>

					</c:when>
					<c:otherwise>
						<li><a href="${ pageContext.request.contextPath }/search">Sale Ad</a></li>
						<li><a href="${ pageContext.request.contextPath }/login">Login</a></li>
						<li><a href="${ pageContext.request.contextPath }/register">Register</a></li>
					</c:otherwise>
				</c:choose>
			</ul>
			<form id="formSearch" class="navbar-form navbar-right" action="${ pageContext.request.contextPath }/search">
				<div class="form-group">
					<select class="form-control" name="searchCategorie" id="searchCategorie">
						<c:choose>
							<c:when test="${requestScope.searchCategorie == -10}">
								<option value="-10" selected>Not selected</option>
							</c:when>
							<c:otherwise>
								<option value="-10">Not selected</option>
							</c:otherwise>
						</c:choose>
						<c:choose>
							<c:when test="${requestScope.searchCategorie == -1}">
								<option value="-1" selected>Unrelated</option>
							</c:when>
							<c:otherwise>
								<option value="-1">Unrelated</option>
							</c:otherwise>
						</c:choose>
						<c:forEach var="catego" items="${requestScope.categoriesNavabar}">

							<c:choose>
								<c:when test="${requestScope.searchCategorie == catego.id}">
									<option value="${catego.id}" selected>${catego.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${catego.id}">${catego.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					<input type="text" name="searchText"  id="searchText" class="form-control" placeholder="Search" value="${requestScope.searchText}">
				</div>
				<button type="submit" id="btnSubmitSearchForm" class="btn btn-default">Submit</button>
			</form>
		</div>
	</div>
</nav>
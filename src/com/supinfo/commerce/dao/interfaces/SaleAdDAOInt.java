package com.supinfo.commerce.dao.interfaces;

import com.supinfo.commerce.entities.SaleAd;

public interface SaleAdDAOInt extends DAO<SaleAd> {

}

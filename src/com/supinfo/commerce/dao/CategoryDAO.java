package com.supinfo.commerce.dao;

import com.supinfo.commerce.dao.interfaces.CategoryDAOInt;
import com.supinfo.commerce.entities.Category;

public class CategoryDAO extends AbstractJPA<Category> implements CategoryDAOInt {

	public CategoryDAO() {
		super(Category.class);
	}

}

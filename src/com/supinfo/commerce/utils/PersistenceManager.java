package com.supinfo.commerce.utils;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceManager {

	private static EntityManagerFactory entityManagerF;

	public static EntityManagerFactory getEntityManagerFactory() {
		if (entityManagerF == null) {
			entityManagerF = Persistence.createEntityManagerFactory("3JVA");
		}
		return entityManagerF;
	}

	public static void closeEntityManagerFactory() {
		if (entityManagerF != null && entityManagerF.isOpen())
			entityManagerF.close();
	}
}

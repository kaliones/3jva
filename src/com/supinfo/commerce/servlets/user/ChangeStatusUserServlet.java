package com.supinfo.commerce.servlets.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class changeStatusUserServlet
 */
@WebServlet(name = "changeStatusUser", urlPatterns = { "/admin/user/changestatus" })
public class ChangeStatusUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangeStatusUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDAO userDAO = DAOFactory.getUserDAO();
		User user;
		User userConnected = (User) request.getAttribute("userConnected");
		// if admin, edit all profil
		long idUser = -1;
		if (userConnected.getGroup().getKeyName().equals("ADMIN")) {
			try {
				// if id is long load new user update
				// else update your account admin
				idUser = Long.parseLong(request.getParameter("id"));
				user = userDAO.get(idUser);
				if (user == null) {
					request.getSession().setAttribute("msgError", "User not found.");
					response.sendRedirect(request.getContextPath() + "/admin/users");
					return;
				}
				user.setActivated(!user.getActivated());
				userDAO.update(user);
				request.getSession().setAttribute("msgSuccess", "Account has been updated.");
				response.sendRedirect(request.getContextPath() + "/admin/users");
				return;
			} catch (Exception e) {
				idUser = -1;
				request.getSession().setAttribute("msgError", "User not found.");
				response.sendRedirect(request.getContextPath() + "/admin/users");
				return;
			}
		} else {
			user = userConnected;
		}

		user.setActivated(!user.getActivated());
		userDAO.update(user);
		request.getSession().setAttribute("msgSuccess", "Your account has been updated.");
		response.sendRedirect(request.getContextPath() + "/index");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

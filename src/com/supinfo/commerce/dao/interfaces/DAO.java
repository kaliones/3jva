package com.supinfo.commerce.dao.interfaces;

import java.util.List;

public interface DAO<T> {

	void persist(T t);

	void update(T t);

	T get(long id);

	void remove(T t);

	List<T> getAll();
}

package com.supinfo.commerce.servlets.salead;

import java.awt.Image;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.CategoryDAO;
import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.SaleAdDAO;
import com.supinfo.commerce.entities.SaleAd;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class UpdateSaleAd
 */
@WebServlet(name = "SaleAdUpdate", urlPatterns = { "/auth/salead/update" })
public class UpdateSaleAdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateSaleAdServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SaleAdDAO saleAdDAO = DAOFactory.getSaleAdDAO();
		SaleAd saleAd;
		User userConnected = (User) request.getAttribute("userConnected");
		try {
			long saleAdId = Long.parseLong(request.getParameter("id"));
			saleAd = saleAdDAO.get(saleAdId);
			if (saleAd == null) {
				request.getSession().setAttribute("msgError", "Ad not found.");
				response.sendRedirect(request.getContextPath() + "/auth/saleads/manager");
				return;
			}

			if (!userConnected.getGroup().getKeyName().equals("ADMIN")
					&& userConnected.getId() != saleAd.getSeller().getId()) {

				request.getSession().setAttribute("msgError", "You are not the seller of the ad.");
				response.sendRedirect(request.getContextPath() + "/auth/saleads/manager");
				return;

			}

		} catch (NumberFormatException e) {
			request.getSession().setAttribute("msgError", "Ad not found.");
			response.sendRedirect(request.getContextPath() + "/auth/saleads/manager");
			return;
		}

		CategoryDAO categoDAO = DAOFactory.getCategoryDAO();
		request.setAttribute("saleAd", saleAd);
		request.setAttribute("categories", categoDAO.getAll());
		request.getRequestDispatcher("/auth/salead/update.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Create object Sale Ad with id in dao.
		// if exception return error message
		// else update Sale Ad and redicted to Sale ad Detail
		SaleAdDAO saleAdDAO = DAOFactory.getSaleAdDAO();
		CategoryDAO categoryDAO = DAOFactory.getCategoryDAO();
		Map<String, String> errors = new HashMap<String, String>();
		String name = (String) request.getParameter("name");
		String description = (String) request.getParameter("description");
		User userConnected = (User) request.getAttribute("userConnected");
		String image = (String) request.getParameter("image");
		SaleAd saleAd;
		int categoryId;
		double price;

		try {
			long saleAdId = Long.parseLong(request.getParameter("id"));
			saleAd = saleAdDAO.get(saleAdId);
			if (saleAd == null) {
				request.getSession().setAttribute("msgError", "Ad not found.");
				response.sendRedirect(request.getContextPath() + "/auth/saleads/manager");
				return;
			}

			if (!userConnected.getGroup().getKeyName().equals("ADMIN")
					&& userConnected.getId() != saleAd.getSeller().getId()) {

				request.getSession().setAttribute("msgError", "You are not the seller of the ad.");
				response.sendRedirect(request.getContextPath() + "/auth/saleads/manager");
				return;

			}

		} catch (NumberFormatException e) {
			request.getSession().setAttribute("msgError", "Ad not found.");
			response.sendRedirect(request.getContextPath() + "/auth/saleads/manager");
			return;
		}

		try {
			price = Double.parseDouble(request.getParameter("price"));
		} catch (NumberFormatException e) {
			price = 0;
		}

		if (image == null || image.trim().equals("")) {
			image = "";
		} else {
			try {
				URL url = new URL(image);
				HttpURLConnection huc = (HttpURLConnection) url.openConnection();
				huc.setRequestMethod("HEAD");
				huc.setRequestProperty("User-Agent",
						"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
				huc.connect();
				if (huc.getResponseCode() != HttpURLConnection.HTTP_OK) {
					errors.put("image", "Image not found");
				}
				Image isImage = ImageIO.read(url);
				if (isImage == null) {
					errors.put("image", "The url is not an image.");
				}
			} catch (Exception e) {
				errors.put("image", "Image not found");
			}
		}
		if (name == null || name.trim() == "") {
			errors.put("name", "Name is required");
		} else if (name.length() < 5) {
			errors.put("name", "Name too short.");
		}
		if (description == null || description.trim() == "") {
			errors.put("description", "Description is required");
		} else if (description.length() <= 50) {
			errors.put("description", "Please describe more.");
		}
		if (price <= 0) {
			errors.put("price", "Price is not valid");
		}
		try {
			categoryId = Integer.parseInt(request.getParameter("categories"));
		} catch (NumberFormatException e) {
			categoryId = -1;
		}
		saleAd.setName(name);
		saleAd.setImage(image);
		saleAd.setCategory(categoryDAO.get(categoryId));
		saleAd.setDescription(description);
		saleAd.setPrice(price);

		request.setAttribute("saleAd", saleAd);
		request.setAttribute("errors", errors);
		if (errors.size() > 0) {
			doGet(request, response);
		} else {
			saleAd.setUpdatedAt(new Date());
			saleAdDAO.update(saleAd);
			request.getSession().setAttribute("msgSuccess", "Your ad was updated !");
			if (!userConnected.getGroup().getKeyName().equals("ADMIN")) {
				response.sendRedirect(request.getContextPath() + "/auth/saleads/manager");
			} else {
				response.sendRedirect(request.getContextPath() + "/admin/saleads");
			}
		}
	}

}

package com.supinfo.commerce.servlets.user;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class UpdateUser
 */
@WebServlet(name = "UserUpdate", urlPatterns = { "/auth/user/update" })
public class UpdateUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDAO userDAO = DAOFactory.getUserDAO();
		User user;
		// if admin, edit all profil
		long idUser = -1;
		User userConnected = (User) request.getAttribute("userConnected");
		if (userConnected.getGroup().getKeyName().equals("ADMIN")) {
			try {
				// if id is long load new user edit
				// else edit your account admin
				idUser = Long.parseLong(request.getParameter("id"));
				user = userDAO.get(idUser);
				if (user == null) {
					request.getSession().setAttribute("msgError", "User not found.");
					response.sendRedirect(request.getContextPath() + "/admin/users");
					return;
				}
			} catch (Exception e) {
				idUser = -1;
				user = userConnected;
			}
		} else {
			user = userConnected;
		}
		request.setAttribute("user", user);
		if (idUser != -1) {
			request.setAttribute("id", idUser);
		}
		request.getRequestDispatcher("/auth/user/update.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDAO userDAO = DAOFactory.getUserDAO();
		Map<String, String> errors = new HashMap<String, String>();
		String email = (String) request.getParameter("email");
		String username = (String) request.getParameter("username");
		String lastName = (String) request.getParameter("lastName");
		String firstName = (String) request.getParameter("firstName");
		String phone = (String) request.getParameter("phone");
		String address = (String) request.getParameter("address");

		if (email == null || email.trim() == "") {
			errors.put("email", "Email address is required");
		} else if (!User.isValideEmail(email)) {
			errors.put("email", "Email address is not a valid email address");
		}
		if (lastName == null || lastName.trim() == "") {
			errors.put("lastName", "Last name is required");
		}
		if (firstName == null || firstName.trim() == "") {
			errors.put("firstName", "First name is required");
		}
		if (phone == null || phone.trim() == "") {
			errors.put("phone", "Phone is required");
		} else if (!User.isValidePhone(phone)) {
			errors.put("phone", "Phone is not valid");
		}
		if (address == null || address.trim() == "") {
			errors.put("address", "Address is required");
		}
		User user;
		User userConnected = (User) request.getAttribute("userConnected");
		String sendRedicrect = request.getContextPath() + "/user";
		if (userConnected.getGroup().getKeyName().equals("ADMIN")) {
			long idUser = -1;
			try {
				idUser = Long.parseLong(request.getParameter("id"));
				user = userDAO.get(idUser);
				if (user == null) {
					request.getSession().setAttribute("msgError", "User not found.");
					response.sendRedirect(request.getContextPath() + "/admin/users");
					return;
				}
			} catch (Exception e) {
				user = userConnected;
			}
		} else {
			user = userConnected;
		}
		user.setEmail(email);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setPhone(phone);
		user.setAddress(address);

		request.setAttribute("user", user);
		request.setAttribute("errors", errors);
		if (errors.size() > 0) {
			doGet(request, response);
		} else {
			userDAO.update(user);
			request.getSession().setAttribute("msgSuccess", "Your account has been saved.");
			response.sendRedirect(sendRedicrect);
		}
	}

}

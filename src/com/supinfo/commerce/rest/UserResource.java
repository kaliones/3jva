package com.supinfo.commerce.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.supinfo.commerce.dao.DAOFactory;

@Path("/user")
public class UserResource {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/count")
	public long getCountInSale() {
		long count = DAOFactory.getUserDAO().countAll();
		return count;
	}

}

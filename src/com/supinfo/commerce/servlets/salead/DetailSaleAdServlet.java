package com.supinfo.commerce.servlets.salead;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.SaleAdDAO;
import com.supinfo.commerce.entities.SaleAd;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class DetailSaleAd
 */
@WebServlet(name = "SaleAd", urlPatterns = { "/salead" })
public class DetailSaleAdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DetailSaleAdServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		SaleAdDAO saleAdDAO = DAOFactory.getSaleAdDAO();
		SaleAd saleAd;
		long idAd;
		User userConnected = (User) request.getAttribute("userConnected");
		try {
			if (request.getParameter("id") != null) {
				idAd = Long.parseLong(request.getParameter("id"));
			} else {
				idAd = Long.parseLong((String) request.getAttribute("id"));
			}
			saleAd = saleAdDAO.get(idAd);
			if (saleAd == null) {
				request.getSession().setAttribute("msgError", "This ad doesn't exist.");
				response.sendRedirect(request.getContextPath() + "/saleads");
				return;
			} else if (!saleAd.isActivated() && (!userConnected.getGroup().getKeyName().equals("ADMIN")
					|| userConnected.getId() != saleAd.getSeller().getId())) {
				request.getSession().setAttribute("msgWarning", "This ad doesn't activated.");
				response.sendRedirect(request.getContextPath() + "/saleads");
				return;
			} else if (saleAd.isSale() && (!userConnected.getGroup().getKeyName().equals("ADMIN")
					|| userConnected.getId() != saleAd.getSeller().getId())) {
				request.getSession().setAttribute("msgWarning", "This ad is sold.");
				response.sendRedirect(request.getContextPath() + "/saleads");
				return;
			}
			request.setAttribute("saleAd", saleAd);
			request.getRequestDispatcher("/auth/salead/detail.jsp").forward(request, response);
		} catch (Exception e) {
			saleAd = null;
			request.getSession().setAttribute("msgError", "This ad doesn't exist.");
			response.sendRedirect(request.getContextPath() + "/saleads");
			return;
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

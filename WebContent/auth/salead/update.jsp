<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../../template/startHTML.jsp"></jsp:include>
<title>Update Sale Ad</title>
</head>
<body>
	<jsp:include page="../../template/navbar.jsp"></jsp:include>
	<jsp:include page="../../template/header.jsp"></jsp:include>
	<div class="row">
		<div class="col-sm-3"></div>
		<form method="post" action="update?id=${saleAd.id}" class="col-sm-6">
			<fieldset>
				<legend class="text-center">Sale Ad</legend>
				<p>You can update your own Sale Ad</p>

				<div class="form-group required	<c:if test="${not empty errors['name']}">has-error</c:if>">
					<label class="control-label" for="name">Ad's Name</label>
					<input class="form-control" type="text" id="name" name="name" value="${saleAd.name}" />
					<span class="help-block">${errors['name']}</span>
				</div>

				<div class="form-group required	<c:if test="${not empty errors['description']}">has-error</c:if>">
					<label class="control-label" for="description">Description</label>
					<input class="form-control" type="text" id="description" name="description" value="${saleAd.description}" />
					<span class="help-block">${errors['description']}</span>
				</div>

				<div class="form-group required	<c:if test="${not empty errors['price']}">has-error</c:if>">
					<label class="control-label" for="price">Price</label>
					<input class="form-control" type="text" id="price" name="price" value="${saleAd.price}">
					<span class="help-block">${errors['price']}</span>
				</div>

				<div class="form-group <c:if test="${not empty errors['image']}">has-error</c:if>">
					<label class="control-label" for="image">URL image</label>
					<input class="form-control" type="text" id="image" name="image" value="${saleAd.image}">
					<span class="help-block">${errors['image']}</span>
				</div>

				<div class="form-group<c:if test="${not empty errors['categories']}">has-error</c:if>">
					<label class="control-label" for="categories">Category</label>
					<select class="form-control" name="categories">
						<option value="-1">Unrelated</option>
						<c:forEach var="catego" items="${categories}">
							<c:choose>
								<c:when test="${ saleAd.category != null && saleAd.category.id == catego.id }">
									<option value="${catego.id}" selected>${catego.name}</option>
								</c:when>
								<c:otherwise>
									<option value="${catego.id}">${catego.name}</option>
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</select>
					<span class="help-block">${errors['categories']}</span>
				</div>

				<input type="submit" value="Publish" class="btn btn-primary pull-right" />
			</fieldset>
		</form>
	</div>
	<jsp:include page="../../template/footer.jsp"></jsp:include>
</body>
</html>
package com.supinfo.commerce.servlets.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.GroupDAO;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.Group;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class changeStatusUserServlet
 */
@WebServlet(name = "changeGroup", urlPatterns = { "/admin/user/changegroup" })
public class ChangeGroupUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ChangeGroupUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDAO userDAO = DAOFactory.getUserDAO();
		User user;
		User userConnected = (User) request.getAttribute("userConnected");
		// if admin
		long idUser = -1;
		if (userConnected.getGroup().getKeyName().equals("ADMIN")) {
			try {
				GroupDAO groupDAO = DAOFactory.getGroupDAO();
				idUser = Long.parseLong(request.getParameter("id"));
				user = userDAO.get(idUser);
				String groupName = (String) request.getParameter("group");
				Group group = groupDAO.getByKey(groupName);
				if (group != null) {
					user.setGroup(group);
					userDAO.update(user);
					request.getSession().setAttribute("msgSuccess", "Account has been updated.");
					response.sendRedirect(request.getContextPath() + "/admin/users");
				} else if (user == null) {
					request.getSession().setAttribute("msgError", "User is not valid.");
					response.sendRedirect(request.getContextPath() + "/admin/users");
				} else {
					request.getSession().setAttribute("msgError", "Group is not valid.");
					response.sendRedirect(request.getContextPath() + "/admin/users");
				}
				return;
			} catch (Exception e) {
				idUser = -1;
			}
		}
		request.getSession().setAttribute("msgError", "You don't have permission.");
		response.sendRedirect(request.getContextPath() + "/index");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}

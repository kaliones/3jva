<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:include page="../../template/startHTML.jsp"></jsp:include>
<title>History Sale Ad</title>
</head>
<body>
	<jsp:include page="../../template/navbar.jsp"></jsp:include>
	<jsp:include page="../../template/header.jsp"></jsp:include>

	<c:if test="${ empty salesAds}">
		<div class="row text-center">
			<h2>This user does not have a sales ad.</h2>
		</div>
	</c:if>
	<div class="row col-sm-12">
		<ul class="pagination pull-right">
			<c:forEach begin="1" end="${nbTotal}" var="pageT">
				<c:if test="${pageT == nbTotal && page <= nbTotal-3}">
					<li><a style="color: gray" href="${ pageContext.request.contextPath }/salead/history?id=${user.id}&page=${nbTotal}">>></a></li>
				</c:if>
				<c:if test="${pageT==1 && 1 <= page-3}">
					<li><a style="color: gray" href="${ pageContext.request.contextPath }/salead/history?id=${user.id}"><<</a></li>
				</c:if>
				<c:if test="${(pageT >= page-2 && pageT <= page+2 )}">

					<c:choose>
						<c:when test="${page eq pageT}">
							<li class="disabled"><a href="#">${pageT}</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="${ pageContext.request.contextPath }/salead/history?id=${user.id}&page=${pageT}">${pageT}</a></li>
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</ul>
	</div>
	<div class="row">
		<c:forEach var="saleAd" items="${salesAds}">
			<div class="row">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">

					<div class="panel <c:choose>
					<c:when test="${saleAd.sale || !saleAd.activated }">
						panel-danger
					</c:when>
					<c:otherwise>
						panel-default
					</c:otherwise>
				</c:choose>">
						<div class="panel-heading  text-center">
							<h2>
								<c:choose>
									<c:when test="${ saleAd.sale }">
									${saleAd.name} <br>(Sold)
								</c:when>
									<c:otherwise>
										<a href="${pageContext.request.contextPath}/salead?id=${saleAd.id }">${saleAd.name}</a>
									</c:otherwise>
								</c:choose>

							</h2>
						</div>
						<div class="panel-body">
							<div class="col-sm-12 row">
								Seller : <a href="${pageContext.request.contextPath}/user?id=${saleAd.seller.id }">${saleAd.seller.username }</a>
							</div>
							<c:if test="${ saleAd.category != null}">
								<div class="col-sm-12 row">Category :</div>
								<div class="col-sm-12 row">${saleAd.category.name }</div>
							</c:if>
							<div class="col-sm-12 row">Price : ${saleAd.price }</div>
							<div class="col-sm-12 row">Description :</div>
							<div class="col-sm-12 row">${saleAd.description }</div>
							<c:if test="${ saleAd.image != null}">
								<div class="col-sm-12">
									<img class="img-responsive center-block" src="${saleAd.image}" alt="${saleAd.name}" />
								</div>
							</c:if>
						</div>
						<div class="panel-footer  text-right">
							Publishing :
							<fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${saleAd.createdAt }" />
							<c:if test="${ saleAd.updatedAt != null}">
						 / Update on : <fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${saleAd.updatedAt }" />
							</c:if>

						</div>
					</div>
				</div>
			</div>
		</c:forEach>
	</div>
	<div class="row  pull-right">
		<ul class="pagination">
			<c:forEach begin="1" end="${nbTotal}" var="pageT">
				<c:if test="${pageT == nbTotal && page <= nbTotal-3}">
					<li><a style="color: gray" href="${ pageContext.request.contextPath }/salead/history?id=${user.id}&page=${nbTotal}">>></a></li>
				</c:if>
				<c:if test="${pageT==1 && 1 <= page-3}">
					<li><a style="color: gray" href="${ pageContext.request.contextPath }/salead/history?id=${user.id}"><<</a></li>
				</c:if>
				<c:if test="${(pageT >= page-2 && pageT <= page+2 )}">

					<c:choose>
						<c:when test="${page eq pageT}">
							<li class="disabled"><a href="#">${pageT}</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="${ pageContext.request.contextPath }/salead/history?id=${user.id}&page=${pageT}">${pageT}</a></li>
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</ul>
	</div>
	<jsp:include page="../../template/footer.jsp"></jsp:include>
</body>
</html>
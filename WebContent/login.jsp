<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="template/startHTML.jsp"></jsp:include>
<title>Login</title>
</head>
<body>
	<jsp:include page="template/navbar.jsp"></jsp:include>
	<jsp:include page="template/header.jsp"></jsp:include>

	<div class="row">
		<div class="col-sm-3"></div>
		<form method="post" action="${ pageContext.request.contextPath }/login" class="col-sm-6">
			<fieldset>
				<legend class="text-center">Login</legend>
				<p>You can login here.</p>

				<div
					class="form-group required	<c:if test="${not empty errors['username']}">has-error</c:if>">
					<label class="control-label" for="username">Username</label> <input
						class="form-control" type="text" id="username" name="username"
						value="${user.username}" /> <span class="help-block">${errors['username']}</span>
				</div>

				<div
					class="form-group required	<c:if test="${not empty errors['password']}">has-error</c:if>">
					<label class="control-label" for="password">Password</label> <input
						class="form-control" type="password" id="password" name="password"
						value="" size="20" maxlength="20" /> <span class="help-block">${errors['password']}</span>
				</div>
				<input type="submit" value="login"
					class="btn btn-primary pull-right" />
			</fieldset>
		</form>
	</div>
	<div class="row text-center">
		<div class="col-sm-3"></div>
		<a href="${ pageContext.request.contextPath }/register">Register</a>
	</div>


	<jsp:include page="template/footer.jsp"></jsp:include>
</body>
</html>
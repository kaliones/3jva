<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:include page="../template/startHTML.jsp"></jsp:include>
<title>Sale Ad Manager</title>
</head>
<body>
	<jsp:include page="../template/navbar.jsp"></jsp:include>
	<jsp:include page="../template/header.jsp"></jsp:include>

	<div class="row">
		<div class="col-sm-12 text-center">
			<h2>Sales Ads Management</h2>
			<table class="table text-center">
				<tr>
					<th class="text-center">Username</th>
					<th class="text-center">Name</th>
					<th class="text-center">Price</th>
					<th class="text-center">Created At</th>
					<th class="text-center">Updated On</th>
					<th class="text-center">Option</th>
				</tr>
				<c:forEach items="${ salesAds }" var="saleAd">
					<tr class="bg-<c:choose><c:when test="${saleAd.activated}">success</c:when><c:otherwise>danger</c:otherwise></c:choose>">
						<td>${ saleAd.seller.username }</td>
						<td>${ saleAd.name }</td>
						<td>${ saleAd.price }</td>
						<td><fmt:formatDate pattern="dd-MM-yyyy" value="${saleAd.createdAt }" /></td>
						<td><fmt:formatDate pattern="dd-MM-yyyy" value="${saleAd.updatedAt }" /></td>
						<td>
							<div class="btn-group" role="group" aria-label="...">
								<a href="${ pageContext.request.contextPath }/salead?id=${saleAd.id}" class="btn btn-info btn-sm">View</a> <a href="${ pageContext.request.contextPath }/auth/salead/update?id=${saleAd.id}" class="btn btn-primary btn-sm">Edit</a>
								<c:choose>
									<c:when test="${saleAd.activated}">
										<a href="${ pageContext.request.contextPath }/auth/salead/changestatus?id=${saleAd.id}" class="btn btn-warning btn-sm">Desactivated</a>

									</c:when>
									<c:otherwise>
										<a href="${ pageContext.request.contextPath }/auth/salead/changestatus?id=${saleAd.id}" class="btn btn-success btn-sm">Activated</a>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${!saleAd.sale}">
										<a href="${ pageContext.request.contextPath }/auth/salead/changesale?id=${saleAd.id}" class="btn btn-warning btn-sm">Set Sold</a>

									</c:when>
									<c:otherwise>
										<a href="${ pageContext.request.contextPath }/auth/salead/changesale?id=${saleAd.id}" class="btn btn-success btn-sm">Set Not Sold</a>
									</c:otherwise>
								</c:choose>
								<a href="${ pageContext.request.contextPath }/auth/salead/remove?id=${saleAd.id}" class="btn btn-danger btn-sm">Delete</a>

							</div>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>

	<div class="row pull-right">
		<ul class="pagination">
			<c:forEach begin="1" end="${nbTotal}" var="pageT">
				<c:if test="${pageT == nbTotal && page <= nbTotal-3}">
					<li><a style="color: gray" href="${ pageContext.request.contextPath }/admin/saleads?page=${nbTotal}">>></a></li>
				</c:if>
				<c:if test="${pageT==1 && 1 <= page-3}">
					<li><a style="color: gray" href="${ pageContext.request.contextPath }/admin/saleads?page=1"><<</a></li>
				</c:if>
				<c:if test="${(pageT >= page-2 && pageT <= page+2 )}">

					<c:choose>
						<c:when test="${page eq pageT}">
							<li class="disabled"><a href="#">${pageT}</a></li>
						</c:when>
						<c:otherwise>
							<li><a href="${ pageContext.request.contextPath }/admin/saleads?page=${pageT}">${pageT}</a></li>
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</ul>
	</div>


	<jsp:include page="../template/footer.jsp"></jsp:include>
</body>
</html>
package com.supinfo.commerce.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.supinfo.commerce.utils.PersistenceManager;

/**
 * Application Lifecycle Listener implementation class ServletContextListener
 *
 */
@WebListener
public class PersistenceListener implements ServletContextListener {

	/**
	 * Default constructor.
	 */
	public PersistenceListener() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see PersistenceListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		PersistenceManager.closeEntityManagerFactory();
	}

	/**
	 * @see PersistenceListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
	}

}

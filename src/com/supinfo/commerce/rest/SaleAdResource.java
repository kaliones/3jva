package com.supinfo.commerce.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.entities.Category;
import com.supinfo.commerce.entities.SaleAd;

@Path("/salead")
public class SaleAdResource {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<SaleAd> getAllSalesAds() {
		List<SaleAd> products = DAOFactory.getSaleAdDAO().getAll();
		return products;
	}

	@GET
	@Path("/{page}/{maxResult}")
	@Produces(MediaType.APPLICATION_JSON)
	public List<SaleAd> getAllSalesAds(@PathParam("page") int page, @PathParam("maxResult") int maxResult) {
		List<SaleAd> salesAds = DAOFactory.getSaleAdDAO().getAllInSale(page, maxResult);
		return salesAds;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public SaleAd getSaleAd(@PathParam("id") long id) {
		SaleAd saleAd = DAOFactory.getSaleAdDAO().get(id);
		return saleAd;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/catego/{idCatego}/{page}/{maxResult}")
	public List<SaleAd> getSaleAdByCategory(@PathParam("idCatego") long id, @PathParam("page") int page,
			@PathParam("maxResult") int maxResult) {
		Category category = DAOFactory.getCategoryDAO().get(id);
		List<SaleAd> salesAds = DAOFactory.getSaleAdDAO().getAllByCategory(category, page, maxResult);
		return salesAds;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/catego/{idCatego}/{page}/{maxResult}/{search}")
	public List<SaleAd> getSaleAdByCategorySearch(@PathParam("idCatego") long id, @PathParam("page") int page,
			@PathParam("maxResult") int maxResult, @PathParam("search") String search) {
		Category category = DAOFactory.getCategoryDAO().get(id);
		List<SaleAd> salesAds = DAOFactory.getSaleAdDAO().getAllByCategory(category, page, maxResult, search);
		return salesAds;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{search}/{page}/{maxResult}")
	public List<SaleAd> getSaleAdBySearch(@PathParam("search") String search, @PathParam("page") int page,
			@PathParam("maxResult") int maxResult) {
		List<SaleAd> salesAds = DAOFactory.getSaleAdDAO().getAllBySearch(search, page, maxResult);
		return salesAds;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/count")
	public long getCountInSale() {
		long count = DAOFactory.getSaleAdDAO().countAllInSale();
		return count;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/countsale")
	public long getCountSale() {
		long count = DAOFactory.getSaleAdDAO().countAllSale();
		return count;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{search}/count")
	public long getCountInSaleBySearch(@PathParam("search") String search) {
		long count = DAOFactory.getSaleAdDAO().countAllInSale(search);
		return count;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/catego/{idCatego}/{search}/count")
	public long getCountInSaleBySearchCategory(@PathParam("idCatego") long id, @PathParam("search") String search) {
		Category category = DAOFactory.getCategoryDAO().get(id);
		long count = 0;
		if (search.equals("")) {
			count = DAOFactory.getSaleAdDAO().countAllInSale(category);
		} else {
			count = DAOFactory.getSaleAdDAO().countAllInSale(category, search);
		}
		return count;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/catego/{idCatego}/count")
	public long getCountInSaleBySearchCategory(@PathParam("idCatego") long id) {
		Category category = DAOFactory.getCategoryDAO().get(id);
		long count = DAOFactory.getSaleAdDAO().countAllInSale(category);
		return count;
	}

}

package com.supinfo.commerce.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import com.supinfo.commerce.dao.interfaces.DAO;
import com.supinfo.commerce.utils.PersistenceManager;

public class AbstractJPA<T> implements DAO<T> {

	protected EntityManagerFactory entityManagerF;

	protected Class<T> classT;

	public AbstractJPA(Class<T> classT) {
		this.classT = classT;
		this.entityManagerF = PersistenceManager.getEntityManagerFactory();
	}

	@Override
	public void persist(T t) {
		EntityManager em = entityManagerF.createEntityManager();
		EntityTransaction tr = em.getTransaction();
		try {
			tr.begin();
			em.persist(t);
			tr.commit();
		} catch (Exception e) {
			tr.rollback();
		} finally {
			em.close();
		}

	}

	@Override
	public void update(T t) {
		EntityManager em = entityManagerF.createEntityManager();
		EntityTransaction tr = em.getTransaction();
		try {
			tr.begin();
			em.merge(t);
			tr.commit();
		} catch (Exception e) {
			tr.rollback();
		} finally {
			em.close();
		}
	}

	@Override
	public T get(long id) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			return em.find(classT, id);
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	@Override
	public void remove(T t) {

		EntityManager em = entityManagerF.createEntityManager();
		EntityTransaction tr = em.getTransaction();
		try {
			tr.begin();
			em.remove(em.contains(t) ? t : em.merge(t));
			tr.commit();
		} catch (Exception e) {
			tr.rollback();
		} finally {
			em.close();
		}
	}

	@Override
	public List<T> getAll() {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			Query query = em.createQuery("SELECT c FROM " + classT.getName() + " c");
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}
}

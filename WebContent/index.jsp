<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="template/startHTML.jsp"></jsp:include>
<title>Home</title>
</head>
<body>
	<jsp:include page="template/navbar.jsp"></jsp:include>
	<jsp:include page="template/header.jsp"></jsp:include>
	<div class="row col-sm-12 text-center">
		<h1>Welcome to Sup Sale</h1>
	</div>
	<div class="row col-sm-12">
		<p> Here you can order products and if you have an account on SupSale you can do more like post your products. In case you are interested in a product there is the possibility of contacting its owner</p>
		<p>There are <span id="nbTotalSaleAd"></span> sales listings on our website.</p>
		<p id="info"><span id="nbTotalSaleAdSale"></span> advertisements have been sold on our site for <span id="nbTotalUser"></span>  registered users.</p>
	</div>
 
 
	
	<div class="row col-sm-12">
		<ul class="pagination pull-right" id="paginateUl">
		</ul>
	</div>
	<div id="content"></div>
	
	<div class="row col-sm-12">
		<ul class="pagination pull-right" id="paginateUl2">
		</ul>
	</div>
	<jsp:include page="template/footer.jsp"></jsp:include>
	<script>

    var maxResult = 12;
    var count = 0;
    function infoRefresh()
    {
    	$.ajax({		
            type: 'GET',
            url : "<c:url value="/rest/salead/"/>countsale",
            timeout: 3000,
            datatype: 'json',
            success: function(data) {
	    		$("#nbTotalSaleAdSale").html(data);	 
	        },
	        error: function() {
	    		$("#info").html("");        	
	        }
	      });  
    	$.ajax({		
            type: 'GET',
            url : "<c:url value="/rest/user/"/>count",
            timeout: 3000,
            datatype: 'json',
            success: function(data) {
	    		$("#nbTotalUser").html(data);
	        },
	        error: function() {
	    		$("#info").html("");
	        }
	      });    
		$("#nbTotalSaleAd").html(count);
    }
    function lastPage()
	{
    	var nbTotal = count/maxResult;
    	nbTotal = Math.ceil(nbTotal);
		page(nbTotal);
	}
    
    function firstPage()
	{
		page(1);
	}
    
    function page(pageValue)
    {
    	 $.ajax({
             type: 'GET',
             url : "<c:url value="/rest/salead/"/>"+pageValue+"/"+maxResult,
             timeout: 3000,
             datatype: 'json',
             success: function(data) {
             	$("#content").empty();
             	$("#paginateUl").empty();
             	$("#paginateUl2").empty();
             	$.each(data, function(key, element) {
             		if(key%4 == 0)
             		{
                     	$("#content").append("<div class='row equal'>");
             		}
               		var name=element.name;
               		name = name.substr(0,22)+"...";
               		var id = element.id;
             		var description = element.description;
             		var price = element.price;
             		var idUser = element.seller.id;
             		var username = element.seller.username;
             		var image = element.image;
                 	var content = '<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">';
                 	content += '<div class="panel panel-default">';
                 	content += '<div class="panel-heading text-center">';
                 	content += '<p style="font-size:18px;font-weight: bold; ">'+name+'</p>';
                 	content += '</div>';
                 	content += '<div class="panel-body" style="padding-top:2px;padding-bottom:2px;">';
                 	content += '<div class="col-sm-12">';
                 	if(image == null || image == "")
                 	{
                 		image = "${ pageContext.request.contextPath }/ressources/no-image.png"
                 	}
                 	content += '<img class="img-responsive center-block" src="'+image+'" width="auto" height="auto"/>';
                 	content += '</div>';
                 	content += '</div>';
                 	content += '<div class="panel-footer  text-right">';
                 	content += '<h4 class="text-left"; float: left; >Price : '+price+' euros</h4>'
                 	content += '<a href="salead?id='+id+'" class="btn btn-info">View More</a>'
                 	content += '</div>';
                 	content += '</div>';
                 	content += '</div>';
                 	$("#content").append(content)
             		if(key%4 == 0)
             		{
                     	$("#content").append("</div>");
             		}
               	}); 
             	paginateAppend(pageValue);
               },
             error: function() {
             	
                }
           });    
    }
    
		function paginateAppend(page)
		{  
			var paginateContent = "";
			$.ajax({		
            type: 'GET',
            url : "<c:url value="/rest/salead/"/>count",
            timeout: 3000,
            datatype: 'json',
            success: function(data) {
            	count = data;
            	var nbTotal =  count/maxResult;
            	nbTotal = Math.ceil(nbTotal);
				for(var pageT = 1; pageT <= nbTotal; pageT++)
				{					
					if(pageT == nbTotal && page  <= nbTotal-3)
					{
						paginateContent += '<li><a style="color: gray" onClick="lastPage()">>></a></li>';
					}
					if(pageT==1 && 1 <= page-3)
					{
						paginateContent += '<li><a style="color: gray" onClick="firstPage()"><<</a></li>';
					}
					if(pageT >= page-2 && pageT <= page+2 )
					{
						if(page == pageT)
						{
							paginateContent +='<li class="disabled"><a href="#">'+pageT+'</a></li>';
						}
						else
						{
							paginateContent +='<li><a onClick="page('+pageT+')">'+pageT+'</a></li>';
						}
					}
				} 
             	$("#paginateUl").empty();
             	$("#paginateUl2").empty();
				$("#paginateUl").append(paginateContent);
				$("#paginateUl2").append(paginateContent);
				infoRefresh();
	        },
	        error: function() {
	        	
	           }
	      });    
		}
		
        $(document).ready(function() {
        	page(1);
        });  

      </script>
</body>
</html>
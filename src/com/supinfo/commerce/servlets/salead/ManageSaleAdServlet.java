package com.supinfo.commerce.servlets.salead;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.SaleAdDAO;
import com.supinfo.commerce.entities.SaleAd;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class ManageSaleAdServlet
 */
@WebServlet(name = "manageSaleAds", urlPatterns = { "/auth/saleads/manager" })
public class ManageSaleAdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public ManageSaleAdServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int page = 1;
		try {
			page = Integer.parseInt(request.getParameter("page"));
		} catch (Exception e) {
		}
		SaleAdDAO saleAdDAO = DAOFactory.getSaleAdDAO();
		User userConnected = (User) request.getAttribute("userConnected");
		List<SaleAd> salesAds = saleAdDAO.getAllByUserActivated(userConnected, page);
		long nbTotal = saleAdDAO.countAll(userConnected);
		request.setAttribute("salesAds", salesAds);
		request.setAttribute("page", page);
		request.setAttribute("nbTotal", nbTotal / 10);
		request.getRequestDispatcher("/auth/salead/manager.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

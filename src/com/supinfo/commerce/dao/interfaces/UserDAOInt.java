package com.supinfo.commerce.dao.interfaces;

import com.supinfo.commerce.entities.User;

public interface UserDAOInt extends DAO<User> {

}

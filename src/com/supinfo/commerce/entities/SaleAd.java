package com.supinfo.commerce.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "salesads")
public class SaleAd implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String name;
	private String description;
	private boolean activated;
	private boolean sale;
	private double price;
	private Date createdAt;
	private Date updatedAt;
	private String image;
	@ManyToOne
	private User seller;

	@ManyToOne
	private Category category;

	public SaleAd() {
	}

	public SaleAd(String name) {
		this.name = name;
	}

	public long getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getDescription() {
		return this.description;
	}

	public double getPrice() {
		return this.price;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public Date getUpdatedAt() {
		return this.updatedAt;
	}

	public Category getCategory() {
		return this.category;
	}

	public String getImage() {
		return this.image;
	}

	public User getSeller() {
		return this.seller;
	}

	public boolean isActivated() {
		return this.activated;
	}

	public boolean isSale() {
		return this.sale;
	}

	public boolean getActivated() {
		return this.activated;
	}

	public boolean getSale() {
		return this.sale;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public void setSale(boolean sale) {
		this.sale = sale;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public void setSeller(User seller) {
		this.seller = seller;
	}
}

package com.supinfo.commerce.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.User;

/**
 * Servlet Filter implementation class AuthentificationFilter
 */
@WebFilter(filterName = "auth", urlPatterns = { "/auth/*" })
public class AuthentificationFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public AuthentificationFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();

		String username = (String) session.getAttribute("username");
		if (username == null) {
			session.setAttribute("msgInfo", "Please login.");
			resp.sendRedirect(req.getContextPath() + "/login");
			return;
		}
		UserDAO userDAO = DAOFactory.getUserDAO();
		User user = userDAO.get(username);
		if (user == null) {
			session.setAttribute("msgInfo", "Please login.");
			resp.sendRedirect(req.getContextPath() + "/login");
			return;
		}
		if (!user.getActivated()) {
			req.getSession().setAttribute("msgError", "Your account is desactivated");
			resp.sendRedirect(req.getContextPath() + "/logout");
			return;
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}

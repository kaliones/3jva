package com.supinfo.commerce.servlets.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class RemoveUser
 */
@WebServlet(name = "UserRemove", urlPatterns = { "/auth/user/remove" })
public class RemoveUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RemoveUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDAO userDAO = DAOFactory.getUserDAO();
		User user;
		// if admin, edit all profil
		long idUser = -1;
		User userConnected = (User) request.getAttribute("userConnected");
		if (userConnected.getGroup().getKeyName().equals("ADMIN")) {
			try {
				// if id is long load new user delete
				// else delete your account admin
				idUser = Long.parseLong(request.getParameter("id"));
				user = userDAO.get(idUser);
				if (user == null) {
					request.getSession().setAttribute("msgError", "User not found.");
					response.sendRedirect(request.getContextPath() + "/admin/users");
					return;
				}
				userDAO.remove(user);
				request.getSession().setAttribute("msgSuccess", "Account has been deleted.");
				response.sendRedirect(request.getContextPath() + "/admin/users");
				return;
			} catch (Exception e) {
				idUser = -1;
				request.getSession().setAttribute("msgError", "User not found.");
				response.sendRedirect(request.getContextPath() + "/admin/users");
				return;
			}
		} else {
			user = userConnected;
		}

		userDAO.remove(user);
		request.getSession().setAttribute("msgSuccess", "Your account has been deleted.");
		response.sendRedirect(request.getContextPath() + "/index");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

package com.supinfo.commerce.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "groups")
public class Group implements Serializable {
	// ADMINISTRATOR("ADMINISTRATOR"), USER("USER");
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private String name;
	private String keyName;

	@OneToMany(mappedBy = "group")
	private List<User> users;

	public Group() {
	}

	public Group(String keyName) {
		this.keyName = keyName;
	}

	public long getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getKeyName() {
		return this.keyName;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setKeyName(String keyName) {
		this.keyName = keyName;
	}
}

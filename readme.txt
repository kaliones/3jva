3JVA project

Description:
Réalisation d'un site permettant à des utilisateurs de poster des annonces et de contacter son auteur.

Username admin: admin
Password admin: admin

Connexion à la base de données obligatoire (voir persistence.xml) 
Par défaut jdbc:mysql://localhost:3306/3JVA avec user: root et mdp : ""

Les catégories, le compte "admin" et les types de groupes sont rajoutés par défaut à la base de données
au démarrage de l'application grâce à un listener.

Membres du groupe : 
- WAROU MARIUS NIMON 216971
- JESSY CHARLOT 222915
- ANCEL ALEXIS 222963


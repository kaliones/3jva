package com.supinfo.commerce.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.supinfo.commerce.dao.CategoryDAO;
import com.supinfo.commerce.dao.CategoryDAO;
import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.User;

/**
 * Servlet Filter implementation class AuthentificationFilter
 */
@WebFilter(filterName = "userConnected", urlPatterns = { "/*" })
public class UserConnectedFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public UserConnectedFilter() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();

		CategoryDAO categoryDAO = DAOFactory.getCategoryDAO();
		String username = (String) session.getAttribute("username");
		if (username != null) {
			UserDAO userDAO = DAOFactory.getUserDAO();
			User user = userDAO.get(username);
			if (user != null) {
				request.setAttribute("userConnected", user);
			}
		}
		request.setAttribute("categoriesNavabar", categoryDAO.getAll());

		chain.doFilter(request, response);

	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/ressources/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${ pageContext.request.contextPath }/ressources/style.css">

<script src="${ pageContext.request.contextPath }/ressources/jquery-2.2.4.min.js"></script>
<script src="${ pageContext.request.contextPath }/ressources/bootstrap.min.js"></script>

package com.supinfo.commerce.dao.interfaces;

import com.supinfo.commerce.entities.Category;

public interface CategoryDAOInt extends DAO<Category> {

}

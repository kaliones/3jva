package com.supinfo.commerce.servlets.user;

import java.io.IOException;
import java.util.Date;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class LoginUserServlet
 */
@WebServlet("/login")
public class LoginUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginUserServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getSession().getAttribute("username") == null) {
			request.getRequestDispatcher("/login.jsp").forward(request, response);
		} else {
			request.getSession().setAttribute("msgInfo", "You are already authenticated.");
			response.sendRedirect(request.getContextPath() + "/index");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		UserDAO userDAO = DAOFactory.getUserDAO();
		User user = userDAO.usernameAuthentificationValide(username, password);
		if (user == null) {
			request.getSession().setAttribute("msgError", "Your username or your password is incorrect");
			RequestDispatcher rd = request.getRequestDispatcher("/login.jsp");
			rd.forward(request, response);
		} else if (!user.getActivated()) {
			request.getSession().setAttribute("msgError", "Your account is desactivated");
			response.sendRedirect(request.getContextPath() + "/index");
			return;
		} else {
			user.setLastConnection(new Date());
			userDAO.update(user);
			request.getSession().setAttribute("msgSuccess", "You are authenticated.");
			request.getSession().setAttribute("username", user.getUsername());
			response.sendRedirect(request.getContextPath() + "/index");
		}

	}

}

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:include page="../../template/startHTML.jsp"></jsp:include>
<title>Detail User</title>
</head>
<body>
	<jsp:include page="../../template/navbar.jsp"></jsp:include>
	<jsp:include page="../../template/header.jsp"></jsp:include>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading  text-center">
					<h2>${user.username}</h2>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-sm-6">First name :</div>
						<div class="col-sm-6">${user.firstName }</div>
					</div>
					<div class="row">
						<div class="col-sm-6">Last name :</div>
						<div class="col-sm-6">${user.lastName }</div>
					</div>
					<div class="row">
						<div class="col-sm-6">Email :</div>
						<div class="col-sm-6">${user.email }</div>
					</div>
					<div class="row">
						<div class="col-sm-6">Phone number :</div>
						<div class="col-sm-6">${user.phone }</div>
					</div>
					<div class="row">
						<div class="col-sm-6">Address :</div>
						<div class="col-sm-6">${user.address }</div>
					</div>
					<div class="col-sm-12 text-center">
						<a class="btn btn-info" href="${pageContext.request.contextPath}/salead/history?id=${user.id}">History</a>
						<c:if test="${(requestScope.userConnected.group.name) != null && (requestScope.userConnected.group.keyName) == 'ADMIN'}">
							<div class="btn-group" role="group" aria-label="...">
								<c:if test="${sessionScope.username != user.username }">
									<c:choose>
										<c:when test="${user.activated}">
											<a href="${ pageContext.request.contextPath }/admin/user/changestatus?id=${user.id}" class="btn btn-warning btn-sm">Desactivated</a>

										</c:when>
										<c:otherwise>
											<a href="${ pageContext.request.contextPath }/admin/user/changestatus?id=${user.id}" class="btn btn-success btn-sm">Activated</a>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${user.group.keyName == 'ADMIN'}">
											<a href="${ pageContext.request.contextPath }/admin/user/changegroup?group=USER&id=${user.id}" class="btn btn-default btn-sm">Set User</a>

										</c:when>
										<c:otherwise>
											<a href="${ pageContext.request.contextPath }/admin/user/changegroup?group=ADMIN&id=${user.id}" class="btn btn-default btn-sm">Set Admin</a>
										</c:otherwise>
									</c:choose>
									<a href="${ pageContext.request.contextPath }/auth/user/remove?id=${user.id}" class="btn btn-danger btn-sm">Delete</a>
								</c:if>
							</div>
						</c:if>
					</div>
				</div>
				<div class="panel-footer  text-right">
					Created At :
					<fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${user.createdAt }" />
					/ Last connection :
					<fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${user.lastConnection }" />
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="../../template/footer.jsp"></jsp:include>
</body>
</html>
package com.supinfo.commerce.servlets.salead;

import java.awt.Image;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.CategoryDAO;
import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.SaleAdDAO;
import com.supinfo.commerce.entities.SaleAd;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class AddSaleAd
 */
@WebServlet(name = "SaleAdAdd", urlPatterns = { "/auth/salead/add" })
public class AddSaleAdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AddSaleAdServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO create form
		CategoryDAO categoDAO = DAOFactory.getCategoryDAO();
		request.setAttribute("categories", categoDAO.getAll());
		request.getRequestDispatcher("/auth/salead/add.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Create object Sale ad with id in dao.
		// if exception return error message
		// else create Sale ad and redirect to detail
		SaleAdDAO saleAdDAO = DAOFactory.getSaleAdDAO();
		CategoryDAO categoryDAO = DAOFactory.getCategoryDAO();
		Map<String, String> errors = new HashMap<String, String>();
		String name = (String) request.getParameter("name");
		String description = (String) request.getParameter("description");
		String image = (String) request.getParameter("image");
		int categoryId;
		double price;
		try {
			price = Double.parseDouble(request.getParameter("price"));
		} catch (NumberFormatException e) {
			price = 0;
		}

		if (image == null || image.trim().equals("")) {
			image = "";
		} else {
			try {
				URL url = new URL(image);
				HttpURLConnection huc = (HttpURLConnection) url.openConnection();
				huc.setRequestMethod("HEAD");
				huc.setRequestProperty("User-Agent",
						"Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.1.2) Gecko/20090729 Firefox/3.5.2 (.NET CLR 3.5.30729)");
				huc.connect();
				if (huc.getResponseCode() != HttpURLConnection.HTTP_OK) {
					errors.put("image", "Image not found");
				}
				Image isImage = ImageIO.read(url);
				if (isImage == null) {
					errors.put("image", "The url is not an image.");
				}
			} catch (Exception e) {
				errors.put("image", "Image not found");
			}
		}
		if (name == null || name.trim() == "") {
			errors.put("name", "Name is required");
		} else if (name.length() < 5) {
			errors.put("name", "Name too short.");
		}
		if (description == null || description.trim() == "") {
			errors.put("description", "Description is required");
		} else if (description.length() <= 50) {
			errors.put("description", "Please describe more.");
		}
		if (price <= 0) {
			errors.put("price", "Price is not valid");
		}
		try {
			categoryId = Integer.parseInt(request.getParameter("categories"));
		} catch (NumberFormatException e) {
			categoryId = -1;
		}
		SaleAd saleAd = new SaleAd(name);
		saleAd.setImage(image);
		saleAd.setCategory(categoryDAO.get(categoryId));
		saleAd.setDescription(description);
		saleAd.setPrice(price);

		request.setAttribute("saleAd", saleAd);
		request.setAttribute("errors", errors);
		if (errors.size() > 0) {
			doGet(request, response);
		} else {
			saleAd.setActivated(true);
			saleAd.setCreatedAt(new Date());
			saleAd.setSale(false);
			saleAd.setSeller((User) request.getAttribute("userConnected"));
			saleAdDAO.persist(saleAd);
			request.getSession().setAttribute("msgSuccess", "Your ad was published successfully !");
			response.sendRedirect(request.getContextPath() + "/auth/saleads/manager");
		}

	}

}

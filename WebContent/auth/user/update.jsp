<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="../../template/startHTML.jsp"></jsp:include>
<title>Update User</title>
</head>
<body>
	<jsp:include page="../../template/navbar.jsp"></jsp:include>
	<jsp:include page="../../template/header.jsp"></jsp:include>

	<div class="row">
		<div class="col-sm-3"></div>
		<form method="post" action="${ pageContext.request.contextPath }/auth/user/update<c:if test="${not empty id}">?id=${id}</c:if>" class="col-sm-6">
			<fieldset>
				<legend class="text-center">Update your account</legend>
				<p>You can update your account via this form.</p>

				<div
					class="form-group required	<c:if test="${not empty errors['email']}">has-error</c:if>">
					<label class="control-label" for="email">E-mail adress</label> <input
						class="form-control" type="email" id="email" name="email"
						value="${user.email}" /> <span class="help-block">${errors['email']}</span>
				</div>

				<div
					class="form-group required	<c:if test="${not empty errors['lastName']}">has-error</c:if>">
					<label class="control-label" for="lastName">Last name</label> <input
						class="form-control" type="text" id="lastName" name="lastName"
						value="${user.lastName}" /> <span class="help-block">${errors['lastName']}</span>
				</div>
				<div
					class="form-group required	<c:if test="${not empty errors['firstName']}">has-error</c:if>">
					<label class="control-label" for="firstName">First name</label> <input
						class="form-control" type="text" id="firstName" name="firstName"
						value="${user.firstName}" /> <span class="help-block">${errors['firstName']}</span>
				</div>

				<div
					class="form-group required	<c:if test="${not empty errors['phone']}">has-error</c:if>">
					<label class="control-label" for="phone">Phone number</label> <input
						class="form-control" type="tel" id="phone" name="phone"
						value="${user.phone}" /> <span class="help-block">${errors['phone']}</span>
				</div>

				<div
					class="form-group required	<c:if test="${not empty errors['address']}">has-error</c:if>">
					<label class="control-label" for="address">Address</label> <input
						class="form-control" type="text" id="address" name="address"
						value="${user.address}" /> <span class="help-block">${errors['address']}</span>
				</div>

				<input type="submit" value="Save" class="btn btn-primary pull-right" />
			</fieldset>
		</form>
	</div>


	<jsp:include page="../../template/footer.jsp"></jsp:include>
</body>
</html>
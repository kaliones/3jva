<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:include page="../../template/startHTML.jsp"></jsp:include>
<title>Add Sale Ad</title>
</head>
<body>
	<jsp:include page="../../template/navbar.jsp"></jsp:include>
	<jsp:include page="../../template/header.jsp"></jsp:include>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<div class="panel panel-default">
				<div class="panel-heading  text-center">
					<h2>${saleAd.name}</h2>
					<c:if test="${((requestScope.userConnected.group.name) != null && (requestScope.userConnected.group.keyName) == 'ADMIN')||requestScope.userConnected.id == saleAd.seller.id}">
						<div class="row">
							<div class="btn-group" role="group" aria-label="...">
								<a href="${ pageContext.request.contextPath }/salead?id=${saleAd.id}" class="btn btn-info btn-sm">View</a> <a href="${ pageContext.request.contextPath }/auth/salead/update?id=${saleAd.id}" class="btn btn-primary btn-sm">Edit</a>
								<c:choose>
									<c:when test="${saleAd.activated}">
										<a href="${ pageContext.request.contextPath }/auth/salead/changestatus?id=${saleAd.id}" class="btn btn-warning btn-sm">Desactivated</a>

									</c:when>
									<c:otherwise>
										<a href="${ pageContext.request.contextPath }/auth/salead/changestatus?id=${saleAd.id}" class="btn btn-success btn-sm">Activated</a>
									</c:otherwise>
								</c:choose>
								<c:choose>
									<c:when test="${!saleAd.sale}">
										<a href="${ pageContext.request.contextPath }/auth/salead/changesale?id=${saleAd.id}" class="btn btn-warning btn-sm">Set Sold</a>

									</c:when>
									<c:otherwise>
										<a href="${ pageContext.request.contextPath }/auth/salead/changesale?id=${saleAd.id}" class="btn btn-success btn-sm">Set Not Sold</a>
									</c:otherwise>
								</c:choose>
								<a href="${ pageContext.request.contextPath }/auth/salead/remove?id=${saleAd.id}" class="btn btn-danger btn-sm">Delete</a>

							</div>
						</div>
					</c:if>
				</div>
				<div class="panel-body">
					<c:if test="${ saleAd.image != null}">
						<div class="col-sm-12">
							<img class="img-responsive center-block" src="${saleAd.image}" alt="${saleAd.name}" />
						</div>
					</c:if>
					<div class="col-sm-12 row">
						Seller : <a href="${pageContext.request.contextPath}/user?id=${saleAd.seller.id }">${saleAd.seller.username }</a>
					</div>
					<c:if test="${ saleAd.category != null}">
						<div class="col-sm-12 row">Category :</div>
						<div class="col-sm-12 row">${saleAd.category.name }</div>
					</c:if>
					<div class="col-sm-12 row">Price : ${saleAd.price }</div>
					<div class="col-sm-12 row">Description :</div>
					<div class="col-sm-12 row">${saleAd.description }</div>

					<div class="col-sm-12 text-center" style="margin-top:10px">
						<button data-toggle="modal" data-target="#sellerModal" class="btn btn-info" id="viewDetailSeller" name="viewDetailSeller">Seller Detail</button>
					</div>
					<div class="col-sm-12 well well-sm" style="margin-top: 10px">
						<form action="${ pageContext.request.contextPath }/contact">
							<fieldset class="text-center">
								<h2>Contact the seller</h2>
							</fieldset>
							<div class="form-group required <c:if test="${not empty errors['name']}">has-error</c:if>">
								<label class="control-label" for="name">Your Name</label>
								<input type="text" class="form-control" id="name" name="name" placeholder="Your name" value="${res.name }" />
								<span class="help-block">${errors['name']}</span>
							</div>
							<div class="form-group required <c:if test="${not empty errors['phone']}">has-error</c:if>">
								<label class="control-label" for="phone">Phone number</label>
								<input type="text" class="form-control" id="phone" name="phone" placeholder="Phone" value="${res.phone }" />
								<span class="help-block">${errors['phone']}</span>
							</div>
							<div class="form-group required <c:if test="${not empty errors['message']}">has-error</c:if>">
								<label class="control-label" for="message">Message</label>
								<textarea rows="3" class="form-control" id="message" name="message" placeholder="Message"> ${res.message }</textarea>
								<span class="help-block">${errors['message']}</span>
							</div>
							<div class="form-group required <c:if test="${not empty errors['email']}">has-error</c:if>">
								<label class="control-label" for="email">Mail</label>
								<input class="form-control" id="email" name="email" placeholder="email" value="${res.email }"></input>
								<span class="help-block">${errors['email']}</span>
							</div>

							<input type="hidden" class="form-control" id="idSaleAd" name="idSaleAd" placeholder="idSaleAd" value="${saleAd.id }" hidden />

							<div class="form-group text-center">
								<button type="submit" class="btn btn-success">Send</button>
							</div>
						</form>
					</div>
				</div>
				<div class="panel-footer  text-right">
					Publishing :
					<fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${saleAd.createdAt }" />
					<c:if test="${ saleAd.updatedAt != null}">
					 / Update on : <fmt:formatDate pattern="dd-MM-yyyy HH:mm:ss" value="${saleAd.updatedAt }" />
					</c:if>

				</div>
			</div>
		</div>
	</div>
	<div id="sellerModal" class="modal fade" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">${saleAd.seller.lastName} ${saleAd.seller.firstName}</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-6">Email :</div>
						<div class="col-sm-6">${saleAd.seller.email }</div>
					</div>
					<div class="row">
						<div class="col-sm-6">Phone number :</div>
						<div class="col-sm-6">${saleAd.seller.phone }</div>
					</div>
					<div class="row">
						<div class="col-sm-6">Address :</div>
						<div class="col-sm-6">${saleAd.seller.address }</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<jsp:include page="../../template/footer.jsp"></jsp:include>
</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<jsp:include page="../template/startHTML.jsp"></jsp:include>
<title>User Manager</title>
</head>
<body>
	<jsp:include page="../template/navbar.jsp"></jsp:include>
	<jsp:include page="../template/header.jsp"></jsp:include>

	<div class="row">
		<div class="col-sm-12 text-center">
			<h2>User Management</h2>
			<table class="table text-center">
				<tr>
					<th class="text-center">Username</th>
					<th class="text-center">First Name</th>
					<th class="text-center">Last Name</th>
					<th class="text-center">Email</th>
					<th class="text-center">Created At</th>
					<th class="text-center">Last Connection</th>
					<th class="text-center">Option</th>
				</tr>
				<c:forEach items="${ users }" var="user">
					<tr
						class="bg-<c:choose><c:when test="${user.activated}">success</c:when><c:otherwise>danger</c:otherwise></c:choose>">
						<td>${ user.username }</td>
						<td>${ user.firstName }</td>
						<td>${ user.lastName }</td>
						<td>${ user.email }</td>
						<td><fmt:formatDate pattern="dd-MM-yyyy"
								value="${user.createdAt }" /></td>
						<td><fmt:formatDate pattern="dd-MM-yyyy"
								value="${user.lastConnection }" /></td>
						<td>
							<div class="btn-group" role="group" aria-label="...">
								<a
									href="${ pageContext.request.contextPath }/user?id=${user.id}"
									class="btn btn-info btn-sm">View</a> <a
									href="${ pageContext.request.contextPath }/auth/user/update?id=${user.id}"
									class="btn btn-primary btn-sm">Edit</a>
								<c:if test="${sessionScope.username != user.username }">
									<c:choose>
										<c:when test="${user.activated}">
											<a
												href="${ pageContext.request.contextPath }/admin/user/changestatus?id=${user.id}"
												class="btn btn-warning btn-sm">Desactivated</a>
										</c:when>
										<c:otherwise>
											<a
												href="${ pageContext.request.contextPath }/admin/user/changestatus?id=${user.id}"
												class="btn btn-success btn-sm">Activated</a>
										</c:otherwise>
									</c:choose>
									<c:choose>
										<c:when test="${user.group.keyName == 'ADMIN'}">
											<a
												href="${ pageContext.request.contextPath }/admin/user/changegroup?group=USER&id=${user.id}"
												class="btn btn-default btn-sm">Set User</a>

										</c:when>
										<c:otherwise>
											<a
												href="${ pageContext.request.contextPath }/admin/user/changegroup?group=ADMIN&id=${user.id}"
												class="btn btn-default btn-sm">Set Admin</a>
										</c:otherwise>
									</c:choose>
									<a
										href="${ pageContext.request.contextPath }/auth/user/remove?id=${user.id}"
										class="btn btn-danger btn-sm">Delete</a>
								</c:if>
							</div>
						</td>
					</tr>
				</c:forEach>
			</table>
		</div>
	</div>

	<div class="row pull-right">
		<ul class="pagination">
			<c:forEach begin="1" end="${nbTotal}" var="pageT">
				<c:if test="${pageT == nbTotal && page <= nbTotal-3}">
					<li><a style="color: gray"
						href="${ pageContext.request.contextPath }/admin/users?page=${nbTotal}">>></a></li>
				</c:if>
				<c:if test="${pageT==1 && 1 <= page-3}">
					<li><a style="color: gray"
						href="${ pageContext.request.contextPath }/admin/users?page=1"><<</a></li>
				</c:if>
				<c:if test="${(pageT >= page-2 && pageT <= page+2 )}">

					<c:choose>
						<c:when test="${page eq pageT}">
							<li class="disabled"><a href="#">${pageT}</a></li>
						</c:when>
						<c:otherwise>
							<li><a
								href="${ pageContext.request.contextPath }/admin/users?page=${pageT}">${pageT}</a></li>
						</c:otherwise>
					</c:choose>
				</c:if>
			</c:forEach>
		</ul>
	</div>
	<jsp:include page="../template/footer.jsp"></jsp:include>
</body>
</html>
package com.supinfo.commerce.dao.interfaces;

import com.supinfo.commerce.entities.Group;

public interface GroupDAOInt extends DAO<Group> {

}

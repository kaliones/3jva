<!-- request.getSession().removeAttribute("msgXXX");  is obligate in this jsp because with listener flashbag are delete before view with sendRedicrect method -->
<div class="container">
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<c:if test="${not empty sessionScope.msgError}">
		<div class="alert alert-danger alert-dismissable">
	  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<c:forEach items="${sessionScope.msgError}" var="m">
				<c:out value="${m}" />
				<br>
			</c:forEach>
			<%  request.getSession().removeAttribute("msgError"); %>
		</div>
	</c:if>
	
	<c:if test="${not empty sessionScope.msgSuccess}">
		<div class="alert alert-success alert-dismissable">
	  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<c:forEach items="${sessionScope.msgSuccess}" var="m">
				<c:out value="${m}" />
				<br>
			</c:forEach>
			<%  request.getSession().removeAttribute("msgSuccess"); %>
		</div>
	</c:if>
	
	<c:if test="${not empty sessionScope.msgWarning}">
		<div class="alert alert-warning alert-dismissable">
	  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<c:forEach items="${sessionScope.msgWarning}" var="m">
				<c:out value="${m}" />
				<br>
			</c:forEach>
			<%  request.getSession().removeAttribute("msgWarning"); %>
		</div>
	</c:if>
	<c:if test="${not empty sessionScope.msgInfo}">
		<div class="alert alert-info alert-dismissable">
	  		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
			<c:forEach items="${sessionScope.msgInfo}" var="m">
				<c:out value="${m}" />
				<br>
			</c:forEach>
			<%  request.getSession().removeAttribute("msgInfo"); %>
		</div>
	</c:if>  

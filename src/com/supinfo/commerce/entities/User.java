package com.supinfo.commerce.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.owlike.genson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class User implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	private String username;
	private String lastName;
	private String firstName;
	private boolean activated;
	private String phone;
	private String email;
	@JsonIgnore
	private String password;
	private String address;
	private Date createdAt;
	private Date lastConnection;

	@ManyToOne
	private Group group;

	@OneToMany(mappedBy = "seller", cascade = CascadeType.REMOVE)
	private List<SaleAd> saleAds;

	public User() {
	}

	public User(String username) {
		this.username = username;
		this.activated = false;
	}

	public long getId() {
		return this.id;
	}

	public String getUsername() {
		return this.username;
	}

	public String getFirstName() {
		return this.firstName;
	}

	public String getLastName() {
		return this.lastName;
	}

	public String getPhone() {
		return this.phone;
	}

	public String getEmail() {
		return this.email;
	}

	public String getAddress() {
		return this.address;
	}

	@JsonIgnore
	public String getPassword() {
		return this.password;
	}

	public Date getCreatedAt() {
		return this.createdAt;
	}

	public Date getLastConnection() {
		return this.lastConnection;
	}

	public Group getGroup() {
		return this.group;
	}

	public boolean getActivated() {
		return this.activated;
	}

	public void setId(long id) {
		this.id = id;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setGroup(Group group) {
		this.group = group;
	}

	public void setActivated(boolean activated) {
		this.activated = activated;
	}

	public void setCreatedAt(Date createdAt) {
		if (this.createdAt == null) {
			this.createdAt = createdAt;
		}
	}

	public void setLastConnection(Date lastConnection) {
		this.lastConnection = lastConnection;
	}

	public static boolean isValideEmail(String email) {
		if (email == null || email.trim() == "") {
			return false;
		}
		if (!email.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}")) {
			return false;
		}
		return true;
	}

	public static boolean isValidePhone(String phone) {
		if (phone == null || phone.trim() == "") {
			return false;
		}
		phone = phone.replace(".", "");
		phone = phone.replace("-", "");
		// EUROP ou USA ou international
		if (phone.matches("\\d{10}") || phone.matches("\\d{9}") || phone.matches("\\+\\d{11}")) {
			return true;
		}
		return false;
	}

}

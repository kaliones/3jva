package com.supinfo.commerce.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.supinfo.commerce.dao.interfaces.UserDAOInt;
import com.supinfo.commerce.entities.User;

public class UserDAO extends AbstractJPA<User> implements UserDAOInt {

	public UserDAO() {
		super(User.class);
	}

	public boolean usernameExist(String username) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			Query query = em.createQuery("SELECT c FROM " + classT.getName() + " c WHERE c.username = :username");
			query.setParameter("username", username);
			return query.getResultList().size() != 0;
		} catch (Exception e) {
			return false;
		} finally {
			em.close();
		}
	}

	public boolean mailExist(String email) {
		EntityManager em = entityManagerF.createEntityManager();
		try {
			Query query = em.createQuery("SELECT c FROM " + classT.getName() + " c WHERE c.email = :email");
			query.setParameter("email", email);
			return query.getResultList().size() != 0;
		} catch (Exception e) {
			return false;
		} finally {
			em.close();
		}
	}

	public User usernameAuthentificationValide(String username, String password) {
		EntityManager em = entityManagerF.createEntityManager();
		Query query = em.createQuery(
				"SELECT c FROM " + classT.getName() + " c WHERE c.username = :username and c.password = :password");
		query.setParameter("username", username);
		query.setParameter("password", password);
		try {
			return (User) query.getSingleResult();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public User get(String username) {
		EntityManager em = entityManagerF.createEntityManager();
		Query query = em.createQuery("SELECT c FROM " + classT.getName() + " c WHERE c.username = :username");
		query.setParameter("username", username);
		try {
			return (User) query.getSingleResult();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public List<User> getAll(int page) {
		EntityManager em = entityManagerF.createEntityManager();

		try {
			int maxResult = 10;
			Query query = em.createQuery("SELECT c FROM " + classT.getName() + " c");
			query.setFirstResult(maxResult * page - maxResult);
			query.setMaxResults(maxResult);
			return query.getResultList();
		} catch (Exception e) {
			return null;
		} finally {
			em.close();
		}
	}

	public long countAll() {
		EntityManager em = entityManagerF.createEntityManager();
		Query query = em.createQuery("SELECT count(*) FROM " + classT.getName() + " c");
		try {
			return (long) query.getSingleResult();
		} catch (Exception e) {
			return 0;
		} finally {
			em.close();
		}
	}
}

package com.supinfo.commerce.servlets.user;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.GroupDAO;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.Group;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class RegisterUser
 */
@WebServlet(name = "UserRegister", urlPatterns = { "/register" })
public class RegisterUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RegisterUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO create form
		// request.getSession().setAttribute("msgError", "test");
		if (request.getSession().getAttribute("username") == null) {
			request.getRequestDispatcher("/register.jsp").forward(request, response);
		} else {
			request.getSession().setAttribute("msgInfo", "You are authenticated.");
			response.sendRedirect(request.getContextPath() + "/index");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserDAO userDAO = DAOFactory.getUserDAO();
		Map<String, String> errors = new HashMap<String, String>();
		String email = (String) request.getParameter("email");
		String username = (String) request.getParameter("username");
		String lastName = (String) request.getParameter("lastName");
		String firstName = (String) request.getParameter("firstName");
		String phone = (String) request.getParameter("phone");
		String address = (String) request.getParameter("address");
		String password = (String) request.getParameter("password");
		String passwordConfirmation = (String) request.getParameter("passwordConfirmation");
		if (password == null || password.trim() == "") {
			errors.put("password", "Password is required");
		} else if (!password.equals(passwordConfirmation)) {
			errors.put("password", "Password and Password confirmation are different");
			errors.put("passwordConfirmation", "Password and Password confirmation are different");
		} else if (password.length() < 5) {
			errors.put("passwordConfirmation", "Password too short, minimum 5 characters.");
		}
		if (username == null || username.trim() == "") {
			errors.put("username", "Username is required");
		} else if (userDAO.usernameExist(username)) {
			errors.put("username", "Another user already uses this username");
		}
		if (email == null || email.trim() == "") {
			errors.put("email", "Email address is required");
		} else if (!User.isValideEmail(email)) {
			errors.put("email", "Email address is not a valid email address");
		} else if (userDAO.mailExist(email)) {
			errors.put("email", "The email address is already in use.");
		}
		if (lastName == null || lastName.trim() == "") {
			errors.put("lastName", "Last name is required");
		}
		if (firstName == null || firstName.trim() == "") {
			errors.put("firstName", "First name is required");
		}
		if (phone == null || phone.trim() == "") {
			errors.put("phone", "Phone is required");
		} else if (!User.isValidePhone(phone)) {
			errors.put("phone", "Phone is not valid");
		}
		if (address == null || address.trim() == "") {
			errors.put("address", "Address is required");
		}
		User user = new User(username);
		user.setEmail(email);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setPassword(passwordConfirmation);
		user.setPhone(phone);
		user.setAddress(address);

		request.setAttribute("user", user);
		request.setAttribute("errors", errors);
		if (errors.size() > 0) {
			doGet(request, response);
		} else {
			user.setActivated(true);
			user.setCreatedAt(new Date());
			GroupDAO groupDAO = DAOFactory.getGroupDAO();
			Group group = groupDAO.getByKey("USER");
			user.setGroup(group);
			userDAO.persist(user);
			request.setAttribute("username", user.getUsername());
			request.setAttribute("password", user.getPassword());
			request.getRequestDispatcher("/login").forward(request, response);
		}
	}

}

package com.supinfo.commerce.servlets.user;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class DetailUser
 */
@WebServlet(name = "User", urlPatterns = { "/user" })
public class DetailUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public DetailUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDAO userDAO = DAOFactory.getUserDAO();
		User user = userDAO.get((String) request.getSession().getAttribute("username"));
		long idUser;
		try {
			// if id is long load new user detail
			// else detail your account
			
			idUser = Long.parseLong(request.getParameter("id"));
			user = userDAO.get(idUser);
			if (user == null) {

				request.getSession().setAttribute("msgError", "User not found.");
				response.sendRedirect(request.getContextPath() + "/index");
				return;
			}
		} catch (Exception e) {
			idUser = -1;
		}
		user.setPassword("");
		request.setAttribute("user", user);
		request.getRequestDispatcher("/auth/user/detail.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

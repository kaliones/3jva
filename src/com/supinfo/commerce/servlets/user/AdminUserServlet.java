package com.supinfo.commerce.servlets.user;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class AdminUserServlet
 */
@WebServlet(name = "admiUsers", urlPatterns = { "/admin/users" })
public class AdminUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public AdminUserServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		int page = 1;
		try {
			page = Integer.parseInt(request.getParameter("page"));
		} catch (Exception e) {
		}
		UserDAO userDAO = DAOFactory.getUserDAO();
		List<User> users = userDAO.getAll(page);
		long nbTotal = userDAO.countAll();
		request.setAttribute("users", users);
		request.setAttribute("page", page);
		request.setAttribute("nbTotal", nbTotal / 10);
		request.getRequestDispatcher("/admin/userManager.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

package com.supinfo.commerce.servlets.salead;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.SaleAdDAO;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.User;

/**
 * Servlet implementation class HistorySaleAdServlet
 */
@WebServlet(name = "SaleAdHistory", urlPatterns = { "/salead/history" })
public class HistorySaleAdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HistorySaleAdServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		UserDAO userDAO = DAOFactory.getUserDAO();
		SaleAdDAO saleAdDAO = DAOFactory.getSaleAdDAO();
		User userConnected = (User) request.getAttribute("userConnected");
		User user;
		try {
			long userId = Long.parseLong(request.getParameter("id"));
			user = userDAO.get(userId);
			if (user == null) {
				request.getSession().setAttribute("msgError", "User not found.");
				response.sendRedirect(request.getContextPath() + "/index");
				return;
			}
		} catch (NumberFormatException e) {
			request.getSession().setAttribute("msgError", "User not found.");
			response.sendRedirect(request.getContextPath() + "/index");
			return;
		}
		int page = 1;
		try {
			page = Integer.parseInt(request.getParameter("page"));
		} catch (Exception e) {
		}
		if ((userConnected != null && userConnected.getGroup().getKeyName().equals("ADMIN"))
				|| (userConnected != null && userConnected.getId() == user.getId())) {
			request.setAttribute("salesAds", saleAdDAO.getAllByUserActivated(user, page));
			long nbTotal = saleAdDAO.countAllActivated(user);
			request.setAttribute("user", user);
			request.setAttribute("page", page);
			request.setAttribute("nbTotal", nbTotal / 10);
		} else {
			request.setAttribute("salesAds", saleAdDAO.getAllByUser(user, page));
			long nbTotal = saleAdDAO.countAll(user);
			request.setAttribute("user", user);
			request.setAttribute("page", page);
			request.setAttribute("nbTotal", nbTotal / 10);
		}

		request.getRequestDispatcher("/auth/salead/history.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

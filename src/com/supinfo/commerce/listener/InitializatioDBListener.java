package com.supinfo.commerce.listener;

import java.util.Date;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.supinfo.commerce.dao.CategoryDAO;
import com.supinfo.commerce.dao.DAOFactory;
import com.supinfo.commerce.dao.GroupDAO;
import com.supinfo.commerce.dao.UserDAO;
import com.supinfo.commerce.entities.Category;
import com.supinfo.commerce.entities.User;

/**
 * Application Lifecycle Listener implementation class InitializatioDBListener
 *
 */
@WebListener
public class InitializatioDBListener implements ServletContextListener {

	/**
	 * Default constructor.
	 */
	public InitializatioDBListener() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see ServletContextListener#contextDestroyed(ServletContextEvent)
	 */
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
	}

	/**
	 * @see ServletContextListener#contextInitialized(ServletContextEvent)
	 */
	public void contextInitialized(ServletContextEvent arg0) {
		initGroup();
		initCategory();
		initUser();
	}

	private void initUser() {
		UserDAO userDAO = DAOFactory.getUserDAO();
		if (!userDAO.usernameExist("admin")) {
			GroupDAO groupDAO = DAOFactory.getGroupDAO();
			User user = new User("admin");
			user.setActivated(true);
			user.setEmail("222963@supinfo.com");
			user.setFirstName("Ancel");
			user.setLastName("Alexis");
			user.setPassword("admin");
			user.setAddress("not address");
			user.setPhone("0666666666");
			user.setActivated(true);
			user.setCreatedAt(new Date());
			user.setGroup((com.supinfo.commerce.entities.Group) groupDAO.getByKey("ADMIN"));
			userDAO.persist(user);
		}
	}

	private void initCategory() {
		CategoryDAO catDAO = DAOFactory.getCategoryDAO();
		String[] categoriesName = { "Vehicles", "Employment", "Real Estate", "Holidays", "Multimedia",
				"Professional Equipment", "Home", "Hobbies", "Services", "Other" };
		List<Category> categories = catDAO.getAll();
		if (categories.size() == 0) {
			for (String catgoName : categoriesName) {
				Category catego = new Category(catgoName);
				catDAO.persist(catego);
			}
		}
	}

	private void initGroup() {
		GroupDAO groupDAO = DAOFactory.getGroupDAO();
		com.supinfo.commerce.entities.Group userGroup = (com.supinfo.commerce.entities.Group) groupDAO.getByKey("USER");
		com.supinfo.commerce.entities.Group adminGroup = (com.supinfo.commerce.entities.Group) groupDAO
				.getByKey("ADMIN");

		if (userGroup == null) {
			userGroup = new com.supinfo.commerce.entities.Group("USER");
			userGroup.setName("User");
			groupDAO.persist(userGroup);
		}
		if (adminGroup == null) {
			adminGroup = new com.supinfo.commerce.entities.Group("ADMIN");
			adminGroup.setName("Administrator");
			groupDAO.persist(adminGroup);
		}
	}

}
